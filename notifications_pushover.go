package main

import (
	"log"

	"github.com/gregdel/pushover"
	"github.com/spf13/viper"
)

func sendPushover(messageTitle string, messageText string) {
	app := pushover.New(viper.GetString(`notifications.pushover.apikey`))
	recipient := pushover.NewRecipient(viper.GetString(`notifications.pushover.userkey`))
	message := pushover.NewMessageWithTitle(messageText, messageTitle)
	_, err := app.SendMessage(message, recipient)

	if err != nil {
		log.Printf(`Unknown error occured while trying to send pushover message. Error: %v`, err)
	}

}
