package main

import "testing"

func Test_returnStatus(t *testing.T) {
	type args struct {
		shortStatus string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: `Status`, args: args{shortStatus: `P`}, want: `printing`},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := returnStatus(tt.args.shortStatus); got != tt.want {
				t.Errorf("returnStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}
